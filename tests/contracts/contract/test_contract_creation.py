import json
import pytest
from rest_framework import status
from rest_framework.test import APIClient
from django.core.exceptions import ObjectDoesNotExist
from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils
from datetime import date
from universities.models import ConsumerUnit
from universities.models import University
from tariffs.models import Distributor
from users.models import CustomUser
from contracts.views import ContractViewSet

ENDPOINT = '/api/contracts/'

@pytest.mark.django_db
class TestContractCreation:
    def setup_method(self):
        self.client = APIClient()
        self.university = University(name='Test University', cnpj='123456789')
        self.university.save()
        self.consumer_unit = ConsumerUnit(university=self.university)
        self.consumer_unit.save()
        self.distributor = Distributor(name='Test Distributor', cnpj='987654321', university=self.university)
        self.distributor.save()
        self.user_dict = dicts_test_utils.super_user_dict_1
        self.user = create_objects_test_utils.create_test_university_user(self.user_dict, self.university)
        self.contract_dict = dicts_test_utils.contract_dict_1
        self.contract = create_objects_test_utils.create_test_contract(self.contract_dict,  self.distributor, self.consumer_unit)
        self.client.login(
            email = self.user_dict['email'], 
            password = self.user_dict['password'])
     

    def test_create_contract_with_existing_consumer_unit(self):
        data = {
            'start_date': self.contract.start_date,
            'tariff_flag': self.contract.tariff_flag,
            'supply_voltage': self.contract.supply_voltage,
            'peak_contracted_demand_in_kw': self.contract.peak_contracted_demand_in_kw,
            'off_peak_contracted_demand_in_kw': self.contract.off_peak_contracted_demand_in_kw,
            'distributor': self.contract.distributor.id,
            'consumer_unit': self.contract.consumer_unit.id,
        }

        response = self.client.post(ENDPOINT, data, format='json')

        assert response.status_code == status.HTTP_200_OK
       

    def test_create_contract_with_non_existing_consumer_unit(self):
        data = {
            'consumer_unit': 999,  
        }

        response = self.client.post(ENDPOINT, data, format='json')

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data == {'error': 'consumer unit does not exist'}
       
    def test_create_contract_without_permission(self):
        
        user_without_permission = CustomUser(email='test2@example.com', type=CustomUser.university_user_type)
        user_without_permission.save()
        self.client.force_authenticate(user=user_without_permission)

        data = {
            'consumer_unit': self.consumer_unit.id,
        }

        response = self.client.post(ENDPOINT, data, format='json')

        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        