import pytest
import re
from unittest.mock import patch, call
from utils.email.send_email import send_email

# Define some valid emails for testing
valid_emails = [
    'test@example.com',
    'test.test@example.com',
    'test.test.test@example.co.uk',

]

def verify_email_is_valid(email):
    email_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

    if re.match(email_regex, email):
        return True
    else:
        raise Exception('Email not valid')

@pytest.mark.parametrize('email', valid_emails)
def test_accepts_valid_email(email):

    verify_email_is_valid(email)



@patch('smtplib.SMTP')
def test_send_email(mock_smtp):
 
    sender_email = 'sender@example.com'
    sender_password = 'password'
    recipient_email = 'recipient@example.com'
    title = 'Title'
    text_body = 'Hello, world!'
    
    send_email(sender_email, sender_password, recipient_email, title, text_body)
